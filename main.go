package main

import (
	"fmt"
	"reconfileservice/recon"

	"github.com/jasonlvhit/gocron"
	"github.com/joho/godotenv"
)

// var wg sync.WaitGroup

func main() {
	err := godotenv.Load()
	if err != nil {
		fmt.Println("enable to load env file..loading through env config.")
		panic(err)
	}
	// fileGenerationTime := os.Getenv("RECON_FILE_GENERATION_CRON_TIME")
	// gocron.Every(1).Day().At(fileGenerationTime).Do(recon.StartFileGeneration)

	gocron.Every(1).Minute().Do(recon.StartFileGeneration)

	<-gocron.Start()
}
