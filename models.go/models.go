package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"reconfileservice/constants"
	"reconfileservice/driver"
	"reconfileservice/types"
)

func GetPartnerSettings(partnerID int) (types.PartnerSettings, error) {

	var partnerSettings types.PartnerSettings
	var fieldsStr string

	db, err := driver.GetMysqlConnection()

	if err != nil {
		fmt.Println("Unable to get sql connection")
		return partnerSettings, err
	}

	err = db.QueryRow("select fields, file_delimiter, file_type from reconciliation_settings where partner_id = ?", partnerID).Scan(&fieldsStr, &partnerSettings.FileDelimiter, &partnerSettings.FileType)

	if err != nil {
		if err == sql.ErrNoRows {
			// Fetch default settings if settings not exists for given partner
			return getDefaultSettings()
		} else {
			fmt.Println("Error while getting partner settings")
			return partnerSettings, err
		}
	}

	if fieldsStr != "" {
		err = json.Unmarshal([]byte(fieldsStr), &partnerSettings.Fields)
		if err != nil {
			fmt.Println("Error while Unmarshal fieldsStr")
			fmt.Println(err)
			return partnerSettings, err
		}
	}

	return partnerSettings, nil
}

func getDefaultSettings() (types.PartnerSettings, error) {

	fmt.Println("Fetching Default Settings....")

	var defaultSettings types.PartnerSettings

	db, err := driver.GetMysqlConnection()

	if err != nil {
		fmt.Println("Unable to get sql connection")
		return defaultSettings, err
	}

	rows, err := db.Query("select id, name, priority, header from reconciliation_data_fields")
	if err != nil {
		fmt.Println("Error while fetching default settings")
		fmt.Println(err)
		return defaultSettings, err
	}
	for rows.Next() {
		details := types.FieldSettings{}
		details.IsActive = true
		if err := rows.Scan(
			&details.Id,
			&details.Name,
			&details.Priority,
			&details.Header,
		); err != nil {
			fmt.Println("Error while scan default setting data")
			fmt.Println(err)
		}
		defaultSettings.Fields = append(defaultSettings.Fields, details)
	}

	defaultSettings.FileType = constants.DEFAULT_FILE_TYPE
	defaultSettings.FileDelimiter = constants.DEFAULT_FILE_DELIMITER

	return defaultSettings, nil

}
