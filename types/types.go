package types

type PartnerSettings struct {
	Fields        []FieldSettings `json:"fields"`
	FileDelimiter string          `json:"file_delimiter"`
	FileType      string          `json:"file_type"`
}

type FieldSettings struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Header   string `json:"header"`
	Priority int    `json:"priority"`
	IsActive bool   `json:"is_active"`
}
