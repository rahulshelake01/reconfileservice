package driver

import (
	"database/sql"
	"fmt"
	"os"
	"sync"

	_ "github.com/go-sql-driver/mysql"
)

type MysqlCredential struct {
	UserName string
	Password string
	Host     string
	Database string
}

var once sync.Once
var MysqlDB *sql.DB

func GetMysqlConnection() (*sql.DB, error) {
	var err error
	once.Do(func() {
		var mysqlCredentials MysqlCredential
		mysqlCredentials.UserName = os.Getenv("MYSQL_USER")
		mysqlCredentials.Password = os.Getenv("MYSQL_PASSWORD")
		mysqlCredentials.Host = os.Getenv("MYSQL_HOST")
		mysqlCredentials.Database = os.Getenv("MYSQL_DATABASE")
		MysqlDB, err = sql.Open("mysql", mysqlCredentials.UserName+":"+mysqlCredentials.Password+"@tcp("+mysqlCredentials.Host+")/"+mysqlCredentials.Database)
		fmt.Println("once do", err)
	})
	return MysqlDB, err
}
